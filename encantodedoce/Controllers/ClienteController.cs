﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using encantodedoce.Models;
using PagedList;

namespace encantodedoce.Controllers
{
    public class ClienteController : Controller
    {
        private DBConnect db = new DBConnect();

        // GET: Cliente
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.nmCliente = String.IsNullOrEmpty(sortOrder) ? "nmCliente_desc" : "";
            ViewBag.dsEmail = sortOrder == "dsEmail" ? "dsEmail_desc" : "dsEmail";

            //Paginação
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var cliente = from s in db.cliente
                           select s;

            //Busca
            if (!String.IsNullOrEmpty(searchString))
            {
                cliente = cliente.Where(s => s.nmCliente.Contains(searchString)
                                       || s.dsEmail.Contains(searchString)
                                       || s.nrTelefone.Contains(searchString));
            }

            //Ordenação
            switch (sortOrder)
            {
                case "nmCliente_desc":
                    cliente = cliente.OrderByDescending(s => s.nmCliente);
                    break;
                case "dsEmail":
                    cliente = cliente.OrderBy(s => s.dsEmail);
                    break;
                case "dsEmail_desc":
                    cliente = cliente.OrderByDescending(s => s.dsEmail);
                    break;
                default:
                    cliente = cliente.OrderBy(s => s.nmCliente);
                    break;
            }

            //Exibe 10 registros por página
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(cliente.ToPagedList(pageNumber, pageSize));
        }

        // GET: Cliente/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cliente cliente = await db.cliente.FindAsync(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Cliente/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cliente/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idCliente,nmCliente,nrTelefone,dsEndereco,dsEmail,dtInclusao,dtAlteracao")] cliente cliente)
        {
            if (ModelState.IsValid)
            {
                cliente.dtInclusao = DateTime.Now;

                db.cliente.Add(cliente);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        // GET: Cliente/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cliente cliente = await db.cliente.FindAsync(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idCliente,nmCliente,nrTelefone,dsEndereco,dsEmail,dtInclusao,dtAlteracao")] cliente cliente)
        {
            if (ModelState.IsValid)
            {
                cliente.dtAlteracao = DateTime.Now;

                db.Entry(cliente).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cliente);
        }

        // GET: Cliente/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cliente cliente = await db.cliente.FindAsync(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Cliente/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            cliente cliente = await db.cliente.FindAsync(id);
            db.cliente.Remove(cliente);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
