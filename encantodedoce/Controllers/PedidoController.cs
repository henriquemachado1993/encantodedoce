﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using encantodedoce.Models;
using PagedList;

namespace encantodedoce.Controllers
{
    public class PedidoController : Controller
    {
        private DBConnect db = new DBConnect();

        // GET: Pedido
        //public async Task<ActionResult> Index()
        //{
        //    var pedido = db.pedido.Include(p => p.cliente);
        //    return View(await pedido.ToListAsync());
        //}

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.nmCliente = String.IsNullOrEmpty(sortOrder) ? "nmCliente_desc" : "";

            //Paginação
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            db.cliente.Include(p => p.pedidos);

            var pedidos = from p in db.pedido
                           select p;

            //Busca
            if (!String.IsNullOrEmpty(searchString))
            {
                //pedidos = pedidos.Where(p => p..Contains(searchString));
            }

            //Ordenação
            switch (sortOrder)
            {
                case "nmCliente_desc":
                    pedidos = pedidos.OrderByDescending(p => p.idPedido);
                    break;
                default:
                    pedidos = pedidos.OrderBy(p => p.idPedido);
                    break;
            }

            //Exibe 10 registros por página
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(pedidos.ToPagedList(pageNumber, pageSize));

        }

        // GET: Pedido/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pedido pedido = await db.pedido.FindAsync(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            return View(pedido);
        }

        // GET: Pedido/Create
        public ActionResult Create()
        {
            
            ViewBag.idCliente = new SelectList(db.cliente, "idCliente", "nmCliente");
            ViewBag.idProduto = new SelectList(db.produto, "idProduto", "nmProduto");

            return View();
        }

        // POST: Pedido/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idPedido,idCliente,valorTotal,dsPedido,bolPago,dtInclusao,dtAlteracao")] pedido pedido)
        {
            if (ModelState.IsValid)
            {
                pedido.dtInclusao = DateTime.Now;
                
                db.pedido.Add(pedido);
                await db.SaveChangesAsync();
                
                return RedirectToAction("Index");
            }

            ViewBag.idCliente = new SelectList(db.cliente, "idCliente", "nmCliente", pedido.idCliente);
            return View(pedido);
        }

        // GET: Pedido/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pedido pedido = await db.pedido.FindAsync(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            ViewBag.idCliente = new SelectList(db.cliente, "idCliente", "nmCliente", pedido.idCliente);
            return View(pedido);
        }

        // POST: Pedido/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idPedido,idCliente,valorTotal,dsPedido,bolPago,dtInclusao,dtAlteracao")] pedido pedido)
        {
            if (ModelState.IsValid)
            {
                pedido.dtAlteracao = DateTime.Now;

                db.Entry(pedido).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idCliente = new SelectList(db.cliente, "idCliente", "nmCliente", pedido.idCliente);
            return View(pedido);
        }

        // GET: Pedido/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pedido pedido = await db.pedido.FindAsync(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            return View(pedido);
        }

        // POST: Pedido/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            pedido pedido = await db.pedido.FindAsync(id);
            db.pedido.Remove(pedido);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
