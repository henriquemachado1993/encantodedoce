﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using encantodedoce.Models;
using PagedList;

namespace encantodedoce.Controllers
{
    public class TipoProdutoController : Controller
    {
        private DBConnect db = new DBConnect();

        // GET: TipoProdutossss
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.tipoProduto = String.IsNullOrEmpty(sortOrder) ? "tipoProduto" : "";

            //Paginação
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var tipoProdutos = from s in db.tipoProduto
                               select s;

            //Busca
            if (!String.IsNullOrEmpty(searchString))
            {
                tipoProdutos = tipoProdutos.Where(s => s.nmTipoProduto.Contains(searchString));
            }

            //Ordenação
            switch (sortOrder)
            {
                case "tipoProduto":
                    tipoProdutos = tipoProdutos.OrderByDescending(s => s.nmTipoProduto);
                    break;
                default:
                    tipoProdutos = tipoProdutos.OrderBy(s => s.nmTipoProduto);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(tipoProdutos.ToPagedList(pageNumber, pageSize));
        }

        // GET: TipoProduto/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipoProduto tipoProduto = await db.tipoProduto.FindAsync(id);
            if (tipoProduto == null)
            {
                return HttpNotFound();
            }
            return View(tipoProduto);
        }

        // GET: TipoProduto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoProduto/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idTipoProduto,nmTipoProduto,dtInclusao,dtAlteracao")] tipoProduto tipoProduto)
        {
            if (ModelState.IsValid)
            {
                tipoProduto.dtInclusao = DateTime.Now;

                db.tipoProduto.Add(tipoProduto);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tipoProduto);
        }

        // GET: TipoProduto/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipoProduto tipoProduto = await db.tipoProduto.FindAsync(id);
            if (tipoProduto == null)
            {
                return HttpNotFound();
            }
            return View(tipoProduto);
        }

        // POST: TipoProduto/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idTipoProduto,nmTipoProduto,dtInclusao,dtAlteracao")] tipoProduto tipoProduto)
        {
            if (ModelState.IsValid)
            {
                tipoProduto.dtAlteracao = DateTime.Now;

                db.Entry(tipoProduto).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipoProduto);
        }

        // GET: TipoProduto/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipoProduto tipoProduto = await db.tipoProduto.FindAsync(id);
            if (tipoProduto == null)
            {
                return HttpNotFound();
            }
            return View(tipoProduto);
        }

        // POST: TipoProduto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tipoProduto tipoProduto = await db.tipoProduto.FindAsync(id);
            db.tipoProduto.Remove(tipoProduto);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
