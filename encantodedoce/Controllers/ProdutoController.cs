﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using encantodedoce.Models;
using PagedList;

namespace encantodedoce.Controllers
{
    public class ProdutoController : Controller
    {
        private DBConnect db = new DBConnect();

        // GET: Produto
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.nmProduto = String.IsNullOrEmpty(sortOrder) ? "nmProduto_desc" : "";

            //Paginação
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            db.produto.Include(p => p.tipoProduto);

            var produtos = from p in db.produto
                           select p;

            //Busca
            if (!String.IsNullOrEmpty(searchString))
            {
                produtos = produtos.Where(p => p.nmProduto.Contains(searchString));
            }

            //Ordenação
            switch (sortOrder)
            {
                case "nmProduto_desc":
                    produtos = produtos.OrderByDescending(p => p.nmProduto);
                    break;
                default:
                    produtos = produtos.OrderBy(p => p.nmProduto);
                    break;
            }

            //Exibe 10 registros por página
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(produtos.ToPagedList(pageNumber, pageSize));

        }

        // GET: Produto/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            produto produto = await db.produto.FindAsync(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // GET: Produto/Create
        public ActionResult Create()
        {
            ViewBag.idTipoProduto = new SelectList(db.tipoProduto, "idTipoProduto", "nmTipoProduto");
            return View();
        }

        // POST: Produto/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idProduto,idTipoProduto,nmProduto,precoProduto,pesoProduto,dsProduto,dtInclusao,dtAlteracao")] produto produto)
        {
            if (ModelState.IsValid)
            {
                produto.dtInclusao = DateTime.Now;

                db.produto.Add(produto);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idTipoProduto = new SelectList(db.tipoProduto, "idTipoProduto", "nmTipoProduto", produto.idTipoProduto);
            return View(produto);
        }

        // GET: Produto/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            produto produto = await db.produto.FindAsync(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            ViewBag.idTipoProduto = new SelectList(db.tipoProduto, "idTipoProduto", "nmTipoProduto", produto.idTipoProduto);
            return View(produto);
        }

        // POST: Produto/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idProduto,idTipoProduto,nmProduto,precoProduto,pesoProduto,dsProduto,dtInclusao,dtAlteracao")] produto produto)
        {
            if (ModelState.IsValid)
            {
                produto.dtAlteracao = DateTime.Now;

                db.Entry(produto).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idTipoProduto = new SelectList(db.tipoProduto, "idTipoProduto", "nmTipoProduto", produto.idTipoProduto);
            return View(produto);
        }

        // GET: Produto/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            produto produto = await db.produto.FindAsync(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // POST: Produto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            produto produto = await db.produto.FindAsync(id);
            db.produto.Remove(produto);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
