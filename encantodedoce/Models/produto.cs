namespace encantodedoce.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("produto")]
    public partial class produto
    {
        public produto()
        {
            pedidosEfetuados = new HashSet<pedidoEfetuado>();
        }

        [Key]
        public int idProduto { get; set; }

        public int? idTipoProduto { get; set; }

        [Required(ErrorMessage = "Campo 'Nome' � obrigat�rio!")]
        [StringLength(150, ErrorMessage = "O campo 'Nome' deve ser uma cadeia de caracteres com um comprimento m�ximo de 50")]
        public string nmProduto { get; set; }

        public decimal precoProduto { get; set; }

        public int? pesoProduto { get; set; }

        [Column(TypeName = "text")]
        public string dsProduto { get; set; }

        public DateTime dtInclusao { get; set; }

        public DateTime? dtAlteracao { get; set; }

        public virtual ICollection<pedidoEfetuado> pedidosEfetuados { get; set; }

        public virtual tipoProduto tipoProduto { get; set; }
    }
}
