namespace encantodedoce.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pedido")]
    public partial class pedido
    {       
        public pedido()
        {
            pedidosEfetuados = new HashSet<pedidoEfetuado>();
        }

        [Key]
        public int idPedido { get; set; }

        public int? idCliente { get; set; }

        public decimal? valorTotal { get; set; }

        [Column(TypeName = "text")]
        public string dsPedido { get; set; }

        public bool? bolPago { get; set; }

        public DateTime dtInclusao { get; set; }

        public DateTime? dtAlteracao { get; set; }

        public virtual cliente cliente { get; set; }
                
        public virtual ICollection<pedidoEfetuado> pedidosEfetuados { get; set; }
    }
}
