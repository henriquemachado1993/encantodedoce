namespace encantodedoce.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cliente")]
    public partial class cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cliente()
        {
            pedidos = new HashSet<pedido>();
        }

        [Key]
        public int idCliente { get; set; }

        [Required(ErrorMessage = "Campo 'Nome' � obrigat�rio!")]
        [StringLength(150, ErrorMessage = "O campo 'Nome' deve ser uma cadeia de caracteres com um comprimento m�ximo de 150")]
        public string nmCliente { get; set; }

        [StringLength(50, ErrorMessage = "O campo 'Telefone' deve ser uma cadeia de caracteres com um comprimento m�ximo de 50")]
        public string nrTelefone { get; set; }

        [Column(TypeName = "text")]
        public string dsEndereco { get; set; }

        [StringLength(150, ErrorMessage = "O campo 'E-mail' deve ser uma cadeia de caracteres com um comprimento m�ximo de 150")]
        public string dsEmail { get; set; }

        public DateTime dtInclusao { get; set; }

        public DateTime? dtAlteracao { get; set; }

        public virtual ICollection<pedido> pedidos { get; set; }
    }
}
