namespace encantodedoce.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("login")]
    public partial class login
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nrCpf { get; set; }

        [Required]
        [StringLength(150)]
        public string nmUser { get; set; }

        [Required]
        [StringLength(150)]
        public string senha { get; set; }

        public DateTime? dtAcesso { get; set; }

        public DateTime dtInclusao { get; set; }

        public DateTime? dtAlteracao { get; set; }
    }
}
