namespace encantodedoce.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pedidoEfetuado")]
    public partial class pedidoEfetuado
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idPedido { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idProduto { get; set; }

        public int? qntProduto { get; set; }

        public virtual pedido pedido { get; set; }

        public virtual produto produto { get; set; }
    }
}
