namespace encantodedoce.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tipoProduto")]
    public partial class tipoProduto
    {
        public tipoProduto()
        {
            produtoes = new HashSet<produto>();
        }

        [Key]
        public int idTipoProduto { get; set; }

        [Required(ErrorMessage = "Campo 'Tipo do produto' � obrigat�rio!")]
        [StringLength(150, ErrorMessage = "O campo 'Tipo do produto' deve ser uma cadeia de caracteres com um comprimento m�ximo de 50")]
        public string nmTipoProduto { get; set; }

        public DateTime dtInclusao { get; set; }

        public DateTime? dtAlteracao { get; set; }

        public virtual ICollection<produto> produtoes { get; set; }
    }
}
