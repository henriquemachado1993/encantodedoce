namespace encantodedoce.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBConnect : DbContext
    {
        public DBConnect()
            : base("name=DBConnect")
        {
        }

        public virtual DbSet<cliente> cliente { get; set; }
        public virtual DbSet<login> login { get; set; }
        public virtual DbSet<pedido> pedido { get; set; }
        public virtual DbSet<produto> produto { get; set; }
        public virtual DbSet<tipoProduto> tipoProduto { get; set; }
        public virtual DbSet<pedidoEfetuado> pedidoEfetuado { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<cliente>()
                .Property(e => e.nmCliente)
                .IsUnicode(false);

            modelBuilder.Entity<cliente>()
                .Property(e => e.nrTelefone)
                .IsUnicode(false);

            modelBuilder.Entity<cliente>()
                .Property(e => e.dsEndereco)
                .IsUnicode(false);

            modelBuilder.Entity<cliente>()
                .Property(e => e.dsEmail)
                .IsUnicode(false);

            modelBuilder.Entity<login>()
                .Property(e => e.nmUser)
                .IsUnicode(false);

            modelBuilder.Entity<login>()
                .Property(e => e.senha)
                .IsUnicode(false);

            modelBuilder.Entity<pedido>()
                .Property(e => e.valorTotal)
                .HasPrecision(12, 2);

            modelBuilder.Entity<pedido>()
                .Property(e => e.dsPedido)
                .IsUnicode(false);

            modelBuilder.Entity<pedido>()
                .HasMany(e => e.pedidoEfetuados)
                .WithRequired(e => e.pedido)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<produto>()
                .Property(e => e.nmProduto)
                .IsUnicode(false);

            modelBuilder.Entity<produto>()
                .Property(e => e.precoProduto)
                .HasPrecision(12, 2);

            modelBuilder.Entity<produto>()
                .Property(e => e.dsProduto)
                .IsUnicode(false);

            modelBuilder.Entity<produto>()
                .HasMany(e => e.pedidosEfetuados)
                .WithRequired(e => e.produto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tipoProduto>()
                .Property(e => e.nmTipoProduto)
                .IsUnicode(false);
        }
    }
}
