USE [dbencantodoce]
GO
/****** Object:  Table [dbo].[cliente]    Script Date: 16/04/2018 15:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cliente](
	[idCliente] [int] IDENTITY(1,1) NOT NULL,
	[nmCliente] [varchar](150) NOT NULL,
	[nrTelefone] [varchar](50) NULL,
	[dsEndereco] [text] NULL,
	[dsEmail] [varchar](150) NULL,
	[dtInclusao] [datetime] NOT NULL,
	[dtAlteracao] [datetime] NULL,
 CONSTRAINT [PK_cliente] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[login]    Script Date: 16/04/2018 15:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login](
	[nrCpf] [int] NOT NULL,
	[nmUser] [varchar](150) NOT NULL,
	[senha] [varchar](150) NOT NULL,
	[dtAcesso] [datetime] NULL,
	[dtInclusao] [datetime] NOT NULL,
	[dtAlteracao] [datetime] NULL,
 CONSTRAINT [PK_login] PRIMARY KEY CLUSTERED 
(
	[nrCpf] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pedido]    Script Date: 16/04/2018 15:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pedido](
	[idPedido] [int] IDENTITY(1,1) NOT NULL,
	[idCliente] [int] NULL,
	[valorTotal] [decimal](12, 2) NULL,
	[dsPedido] [text] NULL,
	[bolPago] [bit] NULL,
	[dtInclusao] [datetime] NOT NULL,
	[dtAlteracao] [datetime] NULL,
 CONSTRAINT [PK_pedido] PRIMARY KEY CLUSTERED 
(
	[idPedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pedidoEfetuado]    Script Date: 16/04/2018 15:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pedidoEfetuado](
	[idPedido] [int] NOT NULL,
	[idProduto] [int] NOT NULL,
	[qntProduto] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[produto]    Script Date: 16/04/2018 15:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[produto](
	[idProduto] [int] IDENTITY(1,1) NOT NULL,
	[idTipoProduto] [int] NULL,
	[nmProduto] [varchar](150) NOT NULL,
	[precoProduto] [decimal](12, 2) NOT NULL,
	[pesoProduto] [int] NULL,
	[dsProduto] [text] NULL,
	[dtInclusao] [datetime] NOT NULL,
	[dtAlteracao] [datetime] NULL,
 CONSTRAINT [PK_produto] PRIMARY KEY CLUSTERED 
(
	[idProduto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipoProduto]    Script Date: 16/04/2018 15:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipoProduto](
	[idTipoProduto] [int] IDENTITY(1,1) NOT NULL,
	[nmTipoProduto] [varchar](150) NOT NULL,
	[dtInclusao] [datetime] NOT NULL,
	[dtAlteracao] [datetime] NULL,
 CONSTRAINT [PK_tipoProduto] PRIMARY KEY CLUSTERED 
(
	[idTipoProduto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[pedido]  WITH CHECK ADD  CONSTRAINT [FK_pedido_cliente] FOREIGN KEY([idCliente])
REFERENCES [dbo].[cliente] ([idCliente])
GO
ALTER TABLE [dbo].[pedido] CHECK CONSTRAINT [FK_pedido_cliente]
GO
ALTER TABLE [dbo].[pedidoEfetuado]  WITH CHECK ADD  CONSTRAINT [FK_pedidoEfetuado_pedido] FOREIGN KEY([idPedido])
REFERENCES [dbo].[pedido] ([idPedido])
GO
ALTER TABLE [dbo].[pedidoEfetuado] CHECK CONSTRAINT [FK_pedidoEfetuado_pedido]
GO
ALTER TABLE [dbo].[pedidoEfetuado]  WITH CHECK ADD  CONSTRAINT [FK_pedidoEfetuado_produto] FOREIGN KEY([idProduto])
REFERENCES [dbo].[produto] ([idProduto])
GO
ALTER TABLE [dbo].[pedidoEfetuado] CHECK CONSTRAINT [FK_pedidoEfetuado_produto]
GO
ALTER TABLE [dbo].[produto]  WITH CHECK ADD  CONSTRAINT [FK_produto_tipoProduto] FOREIGN KEY([idTipoProduto])
REFERENCES [dbo].[tipoProduto] ([idTipoProduto])
GO
ALTER TABLE [dbo].[produto] CHECK CONSTRAINT [FK_produto_tipoProduto]
GO
